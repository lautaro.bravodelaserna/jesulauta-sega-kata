# Kata Sega RX

## Objetivo
La intención de esta kata de RX es que conozcan y practiquen los siguientes operadores de reactive programming:

- Merge
- ThrottleFirst
- Concat
- Select
- Delay
- Where
- Do

## Introducción

Son los años '90 y trabajas para una consultora de video juegos para la plataforma Sega Genesis.
En el último tiempo estuvieron desarrollando un juego llamado "Street of Rage" en el cual el controlas un personaje que lucha con adversarios.
El personaje puede saltar, agacharse, moverse hacia ambos lados, dar patadas, trompadas y bloquear golpes.
El Proyect Manager te entrega un DISKETTE con una librería llamada RX y te pide que sea utilizada.
La librería tiene escasa documentación y el Proyect Manager no tiene ni la mas remota idea de para que es, pero fue un requisito del cliente (en este caso, SEGA).

## Iteración 1

La primer versión de la consola Sega Genesis viene originalmente con un Joystick de 3 botones de acción (A, B y C). Con estos botones el personaje da patadas, piñas y bloquea respectivamente.
La información de los botones proviene en un stream de datos que puede ser leido facilmente desde la aplicación.
Para esta primer entrega se requiere interpretar la información recibida del joystick por medio del stream y luego transformarla en comandos válidos para ser ejecutados por el personaje.
Nuestro compañero de equipo ya empezó la funcionalidad y luego se tomó vacaciones, por lo que te dejó toda la reponsabilidad a ti!
Por suerte, la parte de leer el stream y la parte de convertir un dato del stream en un comando estaría medianamente resuelta, solo falta enganchar las partes para que funcione.

El resultado esperado de nuestra implementación sería el siguiente:

- UP
- PUNCH
- DOWN
- KICK
- RIGHT
- BLOCK
- LEFT
- PUNCH
- PUNCH

## Iteración 2

Llegó el dia en el cual la empresa Sega presentó su nuevo producto, un Joystick Bluetooth con 6 botones (A,B,C,X,Y,Z) compatible con Sega Genesis.
Tu Proyect Manager te pide darle soporte al juevo joystick cuanto antes con el mismo speach de siempre (que se enteró a último momento, que lo necesitamos para ayer, bla bla bla)
Tu compañero de equipo está podrido del PM y renuncio al día siguiente, pero como vos tenés familia no te queda otra que fumartela.

Por otra lado ,la empresa Sega Genesis te brinda un nuevo stream de datos para poder leer los datos del Joystick Bluetooth llamado "BluetoothJoystickStream".

Es importante que le demos soporte simultáneo a ambos joysticks por las siguientes razones:
1 - Queremos que los usuarios con el Joystick de 3 botones puedan jugar a nuestro juego
2 - Queremos que los usuarios con ambos Joysticks puedan precionar los botones al mismo tiempo.

* hay que tener en cuenta que los botones adicionales (X,Y,Z) no deberían realizar ninguna acción.

El resultado esperado de nuestra implementación com ambos joysticks sería el siguiente:

- UP
- DOWN
- PUNCH
- PUNCH
- DOWN
- UP
- KICK
- KICK
- RIGHT
- KICK
- BLOCK
- LEFT
- LEFT
- BLOCK
- PUNCH
- RIGHT
- PUNCH

## Iteración 3
A todo esto, como frutilla del postre, el Proyect Manager logra mejor puntaje dentro del juego que tu.
¿Cómo lo hace? Destruye a los adversarios rapidamente precionando los botones de combate (A, B y C) como un desenfrenado.
Ya tenes los huevos por el piso, por lo tanto un viernes te quedas acampando en la oficina para solucionar este desbalance.
Lo que debes hacer es limitar la cantidad de acciones de combate consecutivas, es decir, no puede pegar trompada y patada en menos de 100 milisegundos.
Esto aplica tanto para patadas, trompadas y bloqueos.

El resultado esperado de nuestra implementación sería el siguiente:

- UP
- DOWN
- PUNCH
- DOWN
- UP
- KICK
- RIGHT
- KICK
- LEFT
- LEFT
- BLOCK
- RIGHT
- PUNCH

using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace Etermax.SegaRx.Domain
{
    public static class InputStreams
    {
        public static IObservable<string> WiredJoystickStream()
        {
            var values = new List<string> {"↑", "A", "↓", "B", "→", "C", "←", "A", "A"};
            return values.Select(v => Observable.Return(v).Delay(TimeSpan.FromMilliseconds(100))).Concat();
        }

        public static IObservable<string> BluetoothJoystickStream()
        {
            var values = new List<string>
            {
                "b:down", "b:a", "b:up", "b:b", "b:b", "b:left", "b:c", "b:right", "b:x", "b:y", "b:z"
            };
            var stream = values.Select(v => Observable.Return(v).Delay(TimeSpan.FromMilliseconds(100))).Concat();
            return stream.Delay(TimeSpan.FromMilliseconds(50));
        }
    }
}
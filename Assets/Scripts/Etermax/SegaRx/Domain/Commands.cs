namespace Etermax.SegaRx.Domain
{
    public class Commands
    {
        public static readonly PlayerCommand UP = new MovementPlayerCommand(MovementCommandType.UP);
        public static readonly PlayerCommand DOWN = new MovementPlayerCommand(MovementCommandType.DOWN);
        public static readonly PlayerCommand LEFT = new MovementPlayerCommand(MovementCommandType.LEFT);
        public static readonly PlayerCommand RIGHT = new MovementPlayerCommand(MovementCommandType.RIGHT);
        public static readonly PlayerCommand PUNCH = new FightingPlayerCommand(FightingCommandType.PUNCH);
        public static readonly PlayerCommand KICK = new FightingPlayerCommand(FightingCommandType.KICK);
        public static readonly PlayerCommand BLOCK = new FightingPlayerCommand(FightingCommandType.BLOCK);
        public static readonly PlayerCommand NONE = new DefaultCommand();
    }

    public class DefaultCommand : PlayerCommand
    {
        public string GetName()
        {
            return "";
        }
    }
}
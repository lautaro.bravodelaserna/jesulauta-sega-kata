namespace Etermax.SegaRx.Domain
{
    public interface PlayerCommand
    {
        string GetName();
    }


    public class MovementPlayerCommand : PlayerCommand
    {
        private readonly MovementCommandType type;

        public MovementPlayerCommand(MovementCommandType type)
        {
            this.type = type;
        }

        public string GetName()
        {
            return type.ToString();
        }
    }

    public class FightingPlayerCommand : PlayerCommand
    {
        private readonly FightingCommandType type;

        public FightingPlayerCommand(FightingCommandType type)
        {
            this.type = type;
        }

        public string GetName()
        {
            return type.ToString();
        }
    }

    public enum MovementCommandType
    {
        DOWN,
        UP,
        LEFT,
        RIGHT
    }

    public enum FightingCommandType
    {
        KICK,
        PUNCH,
        BLOCK
    }
}
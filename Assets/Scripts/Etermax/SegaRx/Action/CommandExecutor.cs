using Etermax.SegaRx.Domain;
using UnityEngine;

namespace Etermax.SegaRx.Action
{
    public static class CommandExecutor
    {
        public static void Execute(PlayerCommand playerCommand)
        {
            Debug.Log(playerCommand.GetName());
        }
    }
}
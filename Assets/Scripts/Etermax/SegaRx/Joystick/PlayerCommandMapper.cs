using Etermax.SegaRx.Domain;

namespace Etermax.SegaRx.Joystick
{
    public interface PlayerCommandMapper
    {
        bool ShouldApply();

        PlayerCommand Apply();
    }

    public class JoystickPlayerCommandMapper : PlayerCommandMapper
    {
        private readonly string data;

        public JoystickPlayerCommandMapper(string data)
        {
            this.data = data;
        }

        public bool ShouldApply()
        {
            return data.StartsWith("b");
        }

        public PlayerCommand Apply()
        {
            if (data == "b:down")
                return Commands.DOWN;
            if (data == "b:up")
                return Commands.UP;
            if (data == "b:left")
                return Commands.LEFT;
            if (data == "b:right")
                return Commands.RIGHT;
            if (data == "b:a")
                return Commands.PUNCH;
            if (data == "b:b")
                return Commands.KICK;
            if (data == "b:c")
                return Commands.BLOCK;

            return Commands.NONE;
        }
    }
    
    public class OnBoardPlayerCommandMapper : PlayerCommandMapper
    {
        private readonly string data;

        public OnBoardPlayerCommandMapper(string data)
        {
            this.data = data;
        }

        public bool ShouldApply()
        {
            return true;
        }

        public PlayerCommand Apply()
        {
            if (data == "↓")
                return Commands.DOWN;
            if (data == "↑")
                return Commands.UP;
            if (data == "←")
                return Commands.LEFT;
            if (data == "→")
                return Commands.RIGHT;
            if (data == "A")
                return Commands.PUNCH;
            if (data == "B")
                return Commands.KICK;
            if (data == "C")
                return Commands.BLOCK;

            return Commands.NONE;
        }
    }
}
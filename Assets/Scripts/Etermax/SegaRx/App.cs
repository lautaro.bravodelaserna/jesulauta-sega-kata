using System;
using System.Collections.Generic;
using System.Linq;
using Etermax.SegaRx.Action;
using Etermax.SegaRx.Domain;
using Etermax.SegaRx.Joystick;
using UniRx;
using UnityEngine;

namespace Etermax.SegaRx
{
    public class App : MonoBehaviour
    {
        private void Awake()
        {
            ReadInputStream()
                .Select(ToPlayerCommand)
                .Where(command => command.GetName().Any())
                .Subscribe(CommandExecutor.Execute);
        }

        private static IObservable<string> ReadInputStream()
        {
            return InputStreams.WiredJoystickStream()
                .Merge(InputStreams.BluetoothJoystickStream());
        }
        
        private static PlayerCommand ToPlayerCommand(string data)
        {
            var commandMapper = new List<PlayerCommandMapper>
            {
                new JoystickPlayerCommandMapper(data),
                new OnBoardPlayerCommandMapper(data)
            };
            return commandMapper
                .First(mapper => mapper.ShouldApply())
                .Apply();
        }
    }
}